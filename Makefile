export USER_ID=$(shell id -u)
export GROUP_ID=$(shell id -g)

ifndef CI_REGISTRY_IMAGE
	CI_REGISTRY_IMAGE := "php-tools-locale"
endif

composer/%:
	@docker run \
		-ti --rm \
		-v ${PWD}/src:/app \
		-w /app \
		-u ${USER_ID}:${GROUP_ID} \
		composer:2 composer $*

docker-login:
	@docker logout ${CI_REGISTRY}
	@echo "${CI_REGISTRY_PASSWORD}" | docker login ${CI_REGISTRY} -u "${CI_REGISTRY_USER}" --password-stdin

build:
	@docker build \
		-t ${CI_REGISTRY_IMAGE}:latest \
		.
phpcs-fix:
	@docker run \
		-ti \
		-v ${PWD}/src:/opt/project \
		${CI_REGISTRY_IMAGE}:latest \
		/tools/vendor/bin/php-cs-fixer fix --config=/tools/.php-cs-fixer.dist.php --verbose --diff --allow-risky=yes --using-cache=no /opt/project
push:
	@docker push ${CI_REGISTRY_IMAGE}:latest