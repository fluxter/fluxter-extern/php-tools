FROM php:8.2-cli-alpine

RUN apk add git

ENV COMPOSER_ALLOW_SUPERUSER=1
COPY --from=composer/composer:2-bin /composer /usr/bin/composer

ADD rootfs /
WORKDIR /tools

RUN set -eux; \
	composer install --prefer-dist --no-dev --no-scripts --no-progress --optimize-autoloader; \
	composer clear-cache
